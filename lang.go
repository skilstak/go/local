package local

import (
	"os"
	"os/exec"
	"strings"
)

// Locale returns the local system Locale (language). LANG environment variable
// on *nix and "Get-Culture | select -exp Name" in powerful on Windows.
func Locale() string {
	lang := os.Getenv("LANG")

	// *nix
	if lang != "" {
		return strings.Split(lang, ".")[0]
	}

	// windows
	cmd := exec.Command("powershell", "Get-Culture | select -exp Name")
	out, err := cmd.Output()
	if err == nil {
		return strings.TrimSpace(string(out))
	}

	// empty
	return lang
}

// Lang retuns the language of the local system converted to that commonly used
// for the lang attribute of the html tag.
func Lang() string {
	return strings.ReplaceAll(strings.ToLower(Locale()), "_", "-")
}
